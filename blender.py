import bpy
import csv


f = start_frame = 1
frames_per_row = 1 / 100
csv_path = "C:\Coding\SADC.jl\ypr.csv"

ob = bpy.context.object

with open(csv_path) as file:
    rows = csv.reader(file, delimiter=",")
    for row in rows:
        ob.rotation_euler = [float(v) for v in row[1:4]]
        ob.keyframe_insert("rotation_euler", frame=float(row[0]))